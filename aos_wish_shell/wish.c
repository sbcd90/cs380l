//Add your code here
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

char **paths;
int numberOfPath = 100;
int pathCounter = 0;
extern int errno;

void startShell() {
    char shell[10] = "wish> ";
    write(STDOUT_FILENO, shell, strlen(shell));
}

void throwError(int exitStatus) {
    char error_message[30] = "An error has occurred\n";
    write(STDERR_FILENO, error_message, strlen(error_message));
    exit(exitStatus);
}

bool isRedirect(char **tokens, int numTokens) {
    for (int i = 0; i < numTokens; ++i) {
        if (strcmp(tokens[i], ">") == 0) {
            return true;
        }
    }
    return false;
}

char** parseCommand(char *input, int *numTokens) {
    char **tokens = calloc(*numTokens, sizeof(char*));

    char *token;
    char delimiter[5] = " ";
    int idx = 0;
    while ((token = strsep(&input, delimiter)) != NULL) {
        token[strcspn(token, "\n")] = 0;
        tokens[idx++] = token;
    }
    *numTokens = idx;
    return tokens;
}

void initializePath() {
    paths = calloc(numberOfPath, sizeof(char*));
    paths[pathCounter++] = "/bin/";
}

void processRedirect(char **tokens, int numTokens) {

}

void processCd(char **tokens, int numTokens) {

}

void processPath(char **tokens, int numTokens) {

}

void processExit(char **tokens, int numTokens) {
    exit(0);
}

pid_t processCommand(char **tokens, int numTokens) {
    pid_t pid = fork();

    if (pid < 0) {
        throwError(1);
    } else if (pid == 0 && pathCounter > 0) {
        for (int i = 0; i < pathCounter; ++i) {
            char *execPath = strdup(paths[i]);
            execPath = strcat(execPath, tokens[0]);

            if (access(execPath, X_OK) == -1) {
                throwError(0);
            }
            if (execv(execPath, tokens) == -1) {
                throwError(0);
            } else {
                free(execPath);
                break;
            }
        }
    } else {
        int status = 0;
        do {
            waitpid(pid, &status, 0);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    return pid;
}

int main(int argc, char **argv) {
    initializePath();
    if (argc == 1) {
        startShell();
    } else if (argc == 2) {
        char *file = strdup(argv[1]);
        free(file);
    } else {
        throwError(1);
    }

    char *input = NULL;
    size_t size = 0;
    ssize_t read;

    while ((read = getline(&input, &size, stdin)) != -1) {
        if (input != NULL) {
            int numTokens = 100;
            char **tokens = parseCommand(input, &numTokens);
            
            if (isRedirect(tokens, numTokens)) {
                processRedirect(tokens, numTokens);
            } else {
                if (numTokens > 0) {
                    if (strcmp(tokens[0], "cd") == 0) {
                        processCd(tokens, numTokens);
                    } else if (strcmp(tokens[0], "path") == 0) {
                        processPath(tokens, numTokens);
                    } else if (strcmp(tokens[0], "exit") == 0) {
                        processExit(tokens, numTokens);
                    } else {
                        processCommand(tokens, numTokens);
                    }
                } else {
                    throwError(1);
                }
            }
            free(tokens);
        }
        if (argc == 1) {
            startShell();
        }
    }
    free(input);
}